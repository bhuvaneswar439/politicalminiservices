import Router from 'express';
var multer = require('multer');
import moment from 'moment';


import { INTERNALSERV_ERR_MSG, SUCCESS_MSG, SOMETHING_WENT_MSG, NO_DATA_AVL_MSG, FAILED_MSG, ROLE_ID_MSG } from '../utils/errormsg';
import { APIResponse, ResponseWithObject, ResponseWithArray, ResponseArrayWithPagination } from '../utils/status';
import { problemsSchema } from '../dbmanager/mongodbschemas/problemsSchema';
var router = Router();

var employeeimgpath = "./Assets/"

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, employeeimgpath)
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
  
   var upload = multer({ storage: storage });

   router.post('/problemsDocument', upload.any(), async (req, res) => {
    let body = req.body;
    console.log(body);
    if (!req.files || req.files.length === 0) {
      res.send(new APIResponse(400, 'Image file is required'));
      return;
    }
    const image = req.files[0].filename;
    console.log("image:",image); 
  
    try {
      // Find the user by their user ID and update their profile
      const user = await problemsSchema.findOneAndUpdate(
        { _id: body.userId },
        { file: image },
        { new: true }       
      );
  
      // If the user was updated successfully, send a response with the updated user data
      if (user) {
        res.send(new ResponseWithObject(200, 'problemsDocument updated successfully', image));
      } else {
        res.send(new APIResponse(400, 'No user found with the given ID'));
      }
    } catch (error) {
      console.error(error);
      res.send(new APIResponse(500, 'Internal server error'));
    }
  });



router.post("/problemcreate", async (req, res) => {
    console.log("Service Called ..");
    var body = req.body;
    console.log(body);
    
    if (!body.name || body.name.trim() === "") {
      res.json({ status: 400, Message: "Name should not be empty", data: [] });
    } else if (!body.mobileNumber || body.mobileNumber.trim() === "") {
      res.json({ status: 400, Message: "Mobile Number should not be empty", data: [] });
    } else if (!body.aadharNumber || body.aadharNumber.trim() === "") {
      res.json({ status: 400, Message: "Aadhar Number should not be empty", data: [] });
    } else if (!body.comment || body.comment.trim() === "") {
      res.json({ status: 400, Message: "Comment should not be empty", data: [] });
    } else {
      var Problems = problemsSchema;
      var problemCollection = new Problems({
        name: body.name,
        mobileNumber: body.mobileNumber,
        aadharNumber: body.aadharNumber,
        file: body.file,
        comment: body.comment
      });
  
      problemCollection.save(function (err, results) {
        if (err) {
          console.log(err);
          res.json({ status: 0, Message: "Error occurred while saving the problem", data: [] });
        } else {
          res.json({ status: 200, Message: "Problem registered successfully", data: [] });
        }
      });
    }
  });

  router.get("/getproblems", async (req, res) => {
    try {
      const problems = await problemsSchema.find();
      res.json({ status: 200, Message: "Data retrieved successfully", data: problems });
    } catch (err) {
      console.error(err);
      res.json({ status: 500, Message: "Error occurred while retrieving data", data: [] });
    }
  });


  


  
  

  export = router;
     
    

  

  
  
  
