import Router from 'express';
var multer = require('multer');
import moment from 'moment';


import { INTERNALSERV_ERR_MSG, SUCCESS_MSG, SOMETHING_WENT_MSG, NO_DATA_AVL_MSG, FAILED_MSG, ROLE_ID_MSG } from '../utils/errormsg';
import { APIResponse, ResponseWithObject, ResponseWithArray, ResponseArrayWithPagination } from '../utils/status';
import {employeesSchema} from '../dbmanager/mongodbschemas/employeesSchema';


var router = Router();

var employeeimgpath = "./Assets/"

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, employeeimgpath)
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
  
   var upload = multer({ storage: storage });

router.post('/profileimageupdate', upload.any(), async (req, res) => {
  let body = req.body;
  console.log(body);
  if (!req.files || req.files.length === 0) {
    res.send(new APIResponse(400, 'Image file is required'));
    return;
  }
  const image = req.files[0].filename;
  console.log("image:",image); 

  try {
    // Find the user by their user ID and update their profile
    const user = await employeesSchema.findOneAndUpdate(
      { _id: body.userId },
      { imagename: image },
      { new: true }       
    );

    // If the user was updated successfully, send a response with the updated user data
    if (user) {
      res.send(new ResponseWithObject(200, 'profileimage updated successfully', image));
    } else {
      res.send(new APIResponse(400, 'No user found with the given ID'));
    }
  } catch (error) {
    console.error(error);
    res.send(new APIResponse(500, 'Internal server error'));
  }
});



router.post("/employees",async(req,res)=>{
    var body = req.body;
    console.log("employees service called")
    var Query = { "Status": 1} ;
    employeesSchema.find(Query).sort({_id:-1}).exec(function(err,data) {
        if(err){
            res.send({status:400,Message:INTERNALSERV_ERR_MSG,data:err})
        }else{
            if(data.length>0){
                res.send({status:200,Message:SUCCESS_MSG,data:data})
            }else{
                res.send({status:204,Message:NO_DATA_AVL_MSG,data:[]})
            }
        }
    });
    
});


// get empid details


router.post("/empdetails",async(req,res)=>{
    var body = req.body;
    //check empid existed or not
    var Query = { "empid":body.empid,"Status":1};
    employeesSchema.find(Query,function(err,data) {
        if(err){
            res.send({status:400,Message:INTERNALSERV_ERR_MSG,data:err})
        }else{
            if(data.length>0){
                res.send({status:200,Message:SUCCESS_MSG,data:data})
            }else{
                res.send({status:204,Message:NO_DATA_AVL_MSG,data:[]})
            }
        }
    });
    
});

// get deleted employee list
router.post("/getdeletedemployees",async(req,res)=>{
  var body = req.body;
  //check empid existed or not
  var Query = {"Status":0};
  employeesSchema.find(Query,function(err,data) {
      if(err){
          res.send({status:400,Message:INTERNALSERV_ERR_MSG,data:err})
      }else{
          if(data.length>0){
              res.send({status:200,Message:SUCCESS_MSG,data:data})
          }else{
              res.send({status:204,Message:NO_DATA_AVL_MSG,data:[]})
          }
      }
  });
  
});


// router.post("/imageupload", upload.any(), async (req, res) => {
//   console.log(req.files[0].filename);
//   res.send({status:200,Message:SUCCESS_MSG,data:req.files[0].filename})
  
// })

// emp details update
// router.post("/profileupdate", async (req, res) => {
//   let body = req.body;
//   let image;
//   // if(req.files.length>0){
//   //   image = req.files[0].filename
//   //   console.log("profileupdate service called..", body,req.files[0].filename);
//   // }else{
//   //   image = ""
//   // }
//     try {
//       employeesSchema.updateOne({empid:body.empid},{
//         $set: {
//           firstname: body.firstname,
//           lastname: body.lastname,
//           gender: body.gender,
//           dob:body.dob,
//           "Address.doorno": body.doorno,
//           "Address.village": body.village,
//           "Address.city": body.city,
//           "Address.state": body.state,
//           "Address.pincode": body.pincode,
//           imagename: "",
//           imgpath: employeeimgpath,
//           About:body.About,
//         },
//       }).then(result => {
//         console.log(result);
        
//           res.send(new ResponseWithObject(200, "Profile Updated", result));
//       }).catch(err => {
//           res.send(new APIResponse(400, err));
//       });
//     } catch (error) {
//       console.log(error);
      
//       res.status(400).json({ status: 400, message: INTERNALSERV_ERR_MSG, data: error });
//     }
//   });
//   update profile
router.post("/updateemployee", async (req, res) => {
  let body = req.body;
  if(!body.firstname){
    res.send(new APIResponse(400, "First Name Should not be empty"));
  }else if(!body.lastname){
    res.send(new APIResponse(400, "Last Name Should not be empty"));
  }else if(!body.gender){
    res.send(new APIResponse(400, "Gender Required"));
  }else if(!body.email){
    res.send(new APIResponse(400, "Email Required"));
  }else if(!body.phone){
    res.send(new APIResponse(400, "Mobile Number Required"));
  }else if(!body.role){
    res.send(new APIResponse(400, "Role Required"));
  }
    try {
      employeesSchema.updateOne(
        { empid: body.empid },
        {
          $set: {
            firstname: body.firstname,
            lastname: body.lastname,
            email: body.email,
            phone: body.phone,
            gender: body.gender,
            role: body.role,
            dob:body.dob,
            "role.RoleName":body.RoleName,
            "role.RoleType":body.RoleType,
            "role.Technology":body.Technologies,
            "SalaryDetails.Salary":body.Salary,
            "SalaryDetails.JobType":body.JobType,
            "FeeDetails":body.FeeDetails,
            dateofjoin:body.dateofjoin
          },
        }
      ).then(result => {
          res.send(new ResponseWithObject(200, "Employee Profile Updated", result));
      }).catch(err => {
          res.send(new APIResponse(400, err));
      });
    } catch (error) {
      res.status(400).json({ status: 400, message: INTERNALSERV_ERR_MSG, data: error });
    }
  });

  // emp finance

  
router.post("/updateempfinance", async (req, res) => {
  try {
    console.log("Service Called..");
    const body = req.body;
    console.log(body);
    const employees = await employeesSchema.find({
      $and: [{ empid: body.empid }, { "role.RoleName": body.role.RoleName }],
    });
    console.log("Employees found:", employees);
    const employee = employees[0];
    console.log("Employee found:", employee);
    if (!employee) {
      res.status(404).send("Employee not found");
      return;
    }

    console.log("Employee found", employee);

    if (body.role.RoleName === "Trainee") {
      // calculate pending fee for all terms
      const totalPaidFee = body.FeeDetails.PaidFees.reduce(
        (acc, curr) => acc + parseFloat(curr.PaidAmount),
        0
      );
      const totalPendingFee = body.FeeDetails.TotalFee - totalPaidFee;

      // update pending fees for all terms
      employee.FeeDetails.NoofTerms = body.FeeDetails.NoofTerms;
      employee.FeeDetails.PendingFees = [
        { TermNo: "All", PendingFee: totalPendingFee },
      ];

      // update paid fee and pending fee fields in response
      const paidfee = totalPaidFee.toFixed(2);
      const pendingfee = totalPendingFee.toFixed(2);
      const TotalFee = (totalPaidFee + totalPendingFee).toFixed(2); // add missing parentheses
      employee.FeeDetails.PaidFee = paidfee;
      employee.FeeDetails.PendingFee = pendingfee;
      employee.FeeDetails.TotalFee = TotalFee; // add total fee

      // update paid fee for each term and set PaidStatus to "Paid" if PaidAmount is greater than zero
      const paidFees = body.FeeDetails.PaidFees.map((term) => {
        const termName = `Term${term.TermNo}PaidFee`;
        employee.FeeDetails[termName] = term.PaidAmount;
        employee.FeeDetails[`${termName}PaidDate`] = new Date().toISOString();
        const paidStatus = term.PaidAmount > 0 ? "Paid" : "Unpaid";
        employee.FeeDetails[`${termName}PaidStatus`] = paidStatus; // update field name
        const message = paidStatus === "Paid" ? `${termName} is paid` : `${termName} is pending`;
        return {
          TermNo: term.TermNo,
          PaidAmount: term.PaidAmount,
          PaidDate: new Date().toISOString(),
          PaidStatus: paidStatus,
          message: message
        };
      });

      // add paid fees to response
      employee.FeeDetails.PaidFees = paidFees;

    // update termwise paid fees in response and store in db
      const termwisePaidFees = body.FeeDetails.PaidFees.map((term) => {
      const termName = `Term${term.TermNo}PaidFee`;
      const paidStatus = term.PaidAmount > 0 ? "Paid" : "Unpaid";
      employee.FeeDetails[termName] = term.PaidAmount;
      employee.FeeDetails[`${termName}PaidDate`] = new Date(term.PaidDate).toISOString();
      employee.FeeDetails[`${termName}PaidStatus`] = paidStatus; // update field name
      const message = paidStatus === "Paid" ? `${termName} is paid` : `${termName} is pending`;
      return {
      TermNo: term.TermNo,
      PaidAmount: term.PaidAmount,
      PaidDate: term.PaidDate,
      PaidStatus: paidStatus,
      message: message
  };
    });
      employee.FeeDetails.TermwisePaidFees = termwisePaidFees;


    }
      employee.UpdatedBy = body.UpdatedBy;
      employee.updateddate = Date.now();
      
      const updatedEmployee = await employee.save();
      
      console.log("Employee updated", updatedEmployee);
      const updatedFeeDetails = {
        empid: updatedEmployee.empid,
        NoofTerms: updatedEmployee.FeeDetails.NoofTerms,
        TotalFee: updatedEmployee.FeeDetails.TotalFee,
        PaidStatus: updatedEmployee.FeeDetails.PaidStatus, // <-- add this field
        PaidDate: updatedEmployee.FeeDetails.PaidDate, // <-- add this field
        PaidFees: updatedEmployee.FeeDetails.PaidFees.map((term) => {
          const termName = `Term${term.TermNo}PaidFee`;
          return {
            TermNo: term.TermNo,
            PaidAmount: term.PaidAmount,
            PaidDate: term[`${termName}PaidDate`],
            PaidStatus: term.PaidAmount > 0 ? 'Paid' : 'Unpaid'
          };
        }),
        PendingFees: updatedEmployee.FeeDetails.PendingFees
      }
      // if (body.role.RoleName !== "Trainee") {
      //   employee.SalaryDetails.Salary = req.body.SalaryDetails.Salary;
      //   employee.SalaryDetails.RoleType = req.body.SalaryDetails.RoleType;
      // }
      res.json({
        status: 200,
        Message: "finance added Successfully",
        data: []
      });
    } catch (err) {
      console.log(err);
      res.status(400).json({
        status: 400,
        Message: "Something went wrong",
        data: err.message
      })
      }
    });
  

    router.post("/updatesalarydetails", async (req, res) => {
      try {
        const { empid, SalaryDetails } = req.body;
        const employee = await employeesSchema.findOne({ empid });
        if (!employee) {
          res.status(404).send("Employee not found");
          return;
        }
        employee.SalaryDetails.Salary = SalaryDetails.Salary;
        employee.SalaryDetails.RoleType = SalaryDetails.RoleType;
        employee.UpdatedBy = SalaryDetails.UpdatedBy;
        employee.updateddate = Date.now();
        const updatedEmployee = await employee.save();
        console.log("Employee updated", updatedEmployee);
        res.json({
          status: 200,
          Message: "Salary details updated successfully",
          data: []
        });
      } catch (err) {
        console.log(err);
        res.status(400).json({
          status: 400,
          Message: "Something went wrong",
          data: err.message
        });
      }
    });
     
    

  

  
  
  
//  router.post("/updateempfinance", async (req, res) => {
//   try {
//     console.log("Service Called..");
//     var body = req.body;
//     console.log(body);
//     let employees = await employeesSchema.find({
//       $and: [{ empid: body.empid }, { "role.RoleName": body.role.RoleName }],
//     });
//     console.log("Employees found:", employees);
//     let employee = employees[0];
//     console.log("Employee found:", employee);

//     if (!employee) {
//       res.status(404).send("Employee not found");
//       return;
//     }

//     console.log("Employee found", employee);

//     if (body.role.RoleName === "Trainee") {
//       employee.FeeDetails.TotalFee = req.body.FeeDetails.TotalFee;
//       employee.FeeDetails.NoofTerms = req.body.FeeDetails.NoofTerms;
//       employee.FeeDetails.TotalTerms = req.body.FeeDetails.TotalTerms;
//     } else {
    //   employee.SalaryDetails.Salary = req.body.SalaryDetails.Salary;
    //   employee.SalaryDetails.RoleType = req.body.SalaryDetails.RoleType;
    // }

//     let updatedEmployee = await employee.save();

//     console.log("Employee updated", updatedEmployee);
//     res.status(200).send(updatedEmployee);
//   } catch (error) {
//     console.log(error);
//     res.status(500).send(error);
//   }
// });


// router.post("/updateempfinance", async (req, res) => {
//   try {
//     console.log("Service Called..");
//     var body = req.body;
//     console.log(body);
//     let employees = await employeesSchema.find({
//       $and: [{ empid: body.empid }, { "role.RoleName": body.role.RoleName }],
//     });
//     console.log("Employees found:", employees);
//     let employee = employees[0];
//     console.log("Employee found:", employee);

//     if (!employee) {
//       res.status(404).send("Employee not found");
//       return;
//     }

//     console.log("Employee found", employee);

//     if (body.role.RoleName === "Trainee") {
//       employee.FeeDetails.TotalFee = req.body.FeeDetails.TotalFee;
//       employee.FeeDetails.Paidfee = req.body.FeeDetails.Paidfee;
//       employee.FeeDetails.TotalTerms = req.body.FeeDetails.TotalTerms;
      
//       // Update fee status based on number of paid terms
//       let paidTerms = 0;
//       for (let i = 1; i <= employee.FeeDetails.TotalTerms.length; i++) {
//         if (req.body.FeeDetails["Term" + i] === "Paid") {
//           paidTerms++;
//         }
//       }
      
//       if (paidTerms === employee.FeeDetails.NoofTerms) {
//         employee.FeeDetails.feestatus = "Paid";
//       } else {
//         employee.FeeDetails.feestatus = "Partial Payment";
//       }
//     } else {
//       employee.SalaryDetails.Salary = req.body.SalaryDetails.Salary;
//       employee.SalaryDetails.RoleType = req.body.SalaryDetails.RoleType;
//     }

//     let updatedEmployee = await employee.save();

//     console.log("Employee updated", updatedEmployee);
//     res.status(200).send(updatedEmployee);
//   } catch (error) {
//     console.log(error);
//     res.status(500).send(error);
//   }
// });


// get finance 


router.post("/getempfinance",async(req,res)=>{
  var body = req.body;
  //check empid existed or not
  var Query = {"Status":1,};
  employeesSchema.find(Query,function(err,data) {
      if(err){
          res.send({status:400,Message:INTERNALSERV_ERR_MSG,data:err})
      }else{
          if(data.length>0){
              res.send({status:200,Message:SUCCESS_MSG,data:data})
          }else{
              res.send({status:204,Message:NO_DATA_AVL_MSG,data:[]})
          }
      }
  });
  
});


  



    // get happy birth day api


  router.post("/gethbd",async(req,res)=>{
  console.log("Service Called ..")
  var todaydate = moment().format("DD-MM");
  console.log(todaydate);
  
  var thismonth = moment().format("MM");
  employeesSchema.find({"Status":1},{"firstname":1,"surname":1,"dob":1}).exec(function(err,data){
      if(err){
          res.send({status:400,Message:SOMETHING_WENT_MSG,data:err});
      }else{
          if(data.length>0){
              let dobs = [];
              let thismonthbds = [];
              for(let i=0;i<data.length;i++){
                  let datamonth = moment(data[i].dob).format("DD-MM")
                  let thismonthemp = moment(data[i].dob).format("MM")
                  if(thismonth == thismonthemp){
                    thismonthbds.push(data[i])
                  }
                  if(datamonth == todaydate){
                    console.log(datamonth, todaydate);
                    
                      dobs.push(data[i])
                  }
              }
              res.send({status:200,Message:"Data Available..",data:{TodayBirthdays:dobs,ThisMonthBirthdays:thismonthbds}});
          }else{
              res.send({status:204,Message:NO_DATA_AVL_MSG,data:[]});
          }
      }
  });
    
});


router.post('/updateemprole',async(req,res)=>{
  var body = req.body;
  if (!body.empid) {
    res.send(new APIResponse(400, "empid Should not be empty"));
  }else if (!body.role.RoleName) {
    res.send(new APIResponse(400, "RoleName Should not be Empty"));
  }else if (!body.role.Technology) {
    res.send(new APIResponse(400, "Technology Should not be Empty"));
  }else if (!body.phone) {
    res.send(new APIResponse(400, "phone Should not be Empty"));
  }else if (!body.role || !body.role.RoleName) {
    res.send(new APIResponse(400, "RoleName Should not be Empty"));
  }else if (!body.joiningdate || !body.joiningdate) {
      res.send(new APIResponse(400, "joiningdate Should not be Empty"));
  }
console.log(body);

  employeesSchema.updateOne({
      "empid":body.empid,"Status":"1"},
      {
        $set: {
          firstname:body.firstname,
          surname:body.surname,
          email:body.email,
          phone: body.phone,
          joiningdate:body.joiningdate,
          "role.RoleName": body.role.RoleName,
          "role.RoleType": body.role.RoleType,
          "role.startdate": body.role.startdate,
          "role.enddate": body.role.enddate,

          "role.Technology":body.role.Technology,
           UpdatedBy:body.userid

        }
        
        
      },
    ).then(result => {
      res.json({status:200,Message:"Role Updated Successfully",data:[]});
    }).catch(error => {
      res.json({status:400,Message:INTERNALSERV_ERR_MSG,data:error});
    })
});

//  employee delete
router.post('/employeedelete', async(req,res)=>{
  var body = req.body;
  console.log(body);
  if(body.empid == "" || body.empid==null){
      res.json({status:400,Message:"Empid Should not be Empty",data:[]})
  }
  else if(body.comment == "" || body.deletecomment == null) {
      res.json({status:400,Message:"deletecomment should not be empty",data:[]})
  }
  else {
      var Query = { "empid":body.empid } ;
      employeesSchema.updateOne({ "empid":body.empid }, { $set: { Status: "0", deletecomment: body.deletecomment }}, {} , function (err,response) {
          if (err) {
              console.log(err);
              res.json({status:400,Message:"Some thing went wrong",data:err});
          }
          else {
              res.json({status:200,Message:"data deleted successfully.",data:[]});   
          }
      });
  }
});




export = router
