
export const SMSEmailSettings = {

    bcc_email_id: "",

    issendsms: true,
    issendemail: true,

    /* AssetTL EMAIL & SMS Details */
    sendgrid_host: "",
    sendgrid_port: 0,
    sendgrid_id: "",
    sendgrid_password: "",
    from_email_id: "",

    sms_url: "",
    sms_sender_id: "",
    sms_username: "",
    sms_password: "",

}

export const Token = {
    Exists: "existed",
    NotFound: "notfound",
    New: "new"
}
