
import sha256 from 'sha256';
const jwt = require('jsonwebtoken');

export async function getEncryptPassword(password) {
    return sha256.x2(password);
}


export async function generatetoken(uid){
    let jwtSecretKey = "nyc@12345";
	let data = {
		time: Date(),
		userId: uid,
	}

	const token = jwt.sign(data, jwtSecretKey);
    return token;
}
export async function convertdatetimetotimestamp(datetime:any) {
	var dateString = datetime,
    dateTimeParts = dateString.split(' '),
    timeParts:any = dateTimeParts[1].split(':'),
    dateParts:any = dateTimeParts[0].split('/'),
    date;

date = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);
return date.getTime();
}
export async function timediff(intime,outtime){
	var date1 = new Date(intime);
	var date2 = new Date(outtime);
	// 2023-02-14 07:36:23
	var diff = date2.getTime() - date1.getTime();

	var msec = diff;
	var hh = Math.floor(msec / 1000 / 60 / 60);
	msec -= hh*1000*60 * 60;
	var mm = Math.floor(msec / 1000 / 60);
	msec -= mm*1000*60;
	var ss = Math.floor(msec / 1000);
	msec -= ss * 1000;
	let tiemdf = hh + ":" + mm + ":" + ss;
	return tiemdf
}