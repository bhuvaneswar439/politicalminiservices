export class Status {
    public status: any;
    public details: any;
    constructor(status = 200, details = []) {
        this.status = status;
        this.details = details;
    }
}

export class Response {
    public status: any;
    public message: any;
    public details: any;
    constructor(status = 200, message = '', details = []) {
        this.status = status;
        this.message = message;
        this.details = details;
    }
}

export class ResponseWithObject {
    public status: any;
    public message: any;
    public details: any;
    constructor(status = 200, message = '', details = {}) {
        this.status = status;
        this.message = message;
        this.details = details;
    }
}
export class ResponseWithArray {
    public status: any;
    public message: any;
    public details: any;
    constructor(status = 200, message = '', details = []) {
        this.status = status;
        this.message = message;
        this.details = details;
    }
}
export class ResponseArrayWithPagination {
    public status: any;
    public message: any;
    public details: any;
    public pagination: any;
    constructor(status = 200, message = '', details = [], pagination = {
        'pageSize': 0,
        'pageIndex': 1,
        'total': 0,
        'totalPages': 0
    }) {
        this.status = status;
        this.message = message;
        this.details = details;
        this.pagination = pagination
    }
}
export class APIResponse {
    public status: any;
    public message: any;
    constructor(status = 400, message = '') {
        this.status = status;
        this.message = message;
    }
}
export class details {
    public status: any
    public message: any
    public data: any
    constructor(status = 200, message = "", data = {}) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
export class tracker {
    public status: any
    public message: any
    public AssetDetailList: any
    constructor(status, message, AssetDetailList = []) {
        this.status = status;
        this.message = message;
        this.AssetDetailList = AssetDetailList;
    }
}
export class Imsi {
    public status: any
    public message: any
    public IMSI: any
    constructor(status, message, IMSI) {
        this.status = status;
        this.message = message;
        this.IMSI = IMSI;
    }
}



export class LoginResponse {
    status: any
    message: any
    hdata: any
    adata: any
    user_existance: string //tyepes = [new,exist,notfound] 
    widgetsenablelist: any
    accessRights: any;
    featureAvailability: any;
    constructor(status, message, user_existance = "exist", adata = {}, widgetsenablelist = [], accessRights = [], featureAvailability = []) {
        this.status = status
        this.message = message
        this.adata = adata
        this.user_existance = user_existance
        this.widgetsenablelist = widgetsenablelist
        this.accessRights = accessRights
        this.featureAvailability = featureAvailability
    }
}
