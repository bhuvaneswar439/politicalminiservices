export const LOGIN_ID = "Login id is required";
export const CLIENT_ID = "Client id is required";
export const USER_ID = "User id is required";
export const EMP_LOGIN_TIME = "9:45:00"
//Common Messages
export const SUCCESS_MSG = "Success";
export const REMOVE_SUCCESS_MSG = "Remove success";
export const DATA_AVL_MSG = "Data available";
export const NO_DATA_AVL_MSG = "No data available";
export const INVALIDOTP_MSG = "Invalid OTP"
export const FAILED_MSG = "Failed";
export const SOMETHING_WENT_MSG = "Something went wrong, please try again";
export const INTERNALSERV_ERR_MSG = "Internal server error"
export const ALREADY_EXISTS_MSG = "Already exists"
export const UNAUTH_MSG = "Unauthorized request"
export const NOFILE_EXIST_MSG = "No file exist"

export const BAD_REQ = "Bad request"


//Geofence Controller
export const GEOFENCE_ID_MSG = "Geofence id is required"
export const JIO_GEOFENCE_ID_MSG = "IOT geofence id is required"
export const GEOFENCE_NAME_MSG = "Geofence name is reuired"
export const GEOFENCE_NAME_FORMAT_MSG = "Geofence name only allow alphabets and numbers(50)"
export const SAVEMODE_MSG = "Save mode is required "
export const LATITUDE_MSG = "Save mode is required "
export const LONGITUDE_MSG = "Save mode is required "
export const RADIUS_MSG = "Save mode is required "
export const SHAPETYPE_MSG = "Shape type is required "
export const COLOR_MSG = "Color seletion is required "
export const LATLNG_MSG = "Minumum three latlongs are required"

//Whitelist Controller
export const CIWLID_MSG = "CIWLID is required";
export const IMEI_MSG = "IMEI is required";
export const IMSI_MSG = "IMSI is required";

/**Roles */
export const ROLE_NAME_MSG = "Role name is required";
export const ROLE_NAME_VALIDATE_MSG = "RoleName will allow only Alphabets and Numbers,with Minimum Length 2 and Maximum length 50"


/**Alerts */

export const ALERT_TYPE_MSG = "Alert type is required";
export const FROM_TIME_MSG = "From time is required";
export const TO_TIME_MSG = "To time is required";
export const ALERT_ACTIVE_STATUS_MSG = "Alert active status is required";
// export const SMS_ENABLED_MSG = "Mobile is required";
// export const MAIL_ENABLED_MSG = "Email type is required";
export const ALERT_ID_MSG = "Alert id is required";

/** User */
export const NAME = "Name is required";
export const USER_NAME = "User name is required";
export const PWD_MSG = "Password is required";
export const MAIL_MSG = "Email id is required";
export const MOBILE_NO_MSG = "Mobile number is required";
export const EMAIL_OR_MOBILE_MSG = "Either mobile number or email is required";
export const DEFAULT_MAP_MSG = "Default map is required";
export const ROLE_ID_MSG = "Role id is required";
export const PASSWORD_VAILD_MSG = "Enter valid password"
export const LOCATION_CHECK_MSG = "Location check is required "
export const GEOFENCE_CHECK_MSG = "Geofence check is required "
export const OTP_MSG = "OTP is required"
export const NAME_VALIDATE_MSG = "Name should be Alphanumaric"
export const INVALID_MOBILENO_MSG = "Invalid mobile number"
export const MAILID_VALIDATE_MSG = "Invalid email"
/*Service Report*/
export const REPORT_ID_MSG = "Report id is required";
export const EXPORT_TYPE_MSG = "Export type is required";

/*Site Module*/
export const FEATURE_IDS_MSG = "Feature id's are required"
export const DEFAULT_FEATURE_ID__MSG = "Default Feature id is required"

// trackercontroller

export const ASSET_NAME = 'Tag name is required';
export const ASSET_TYPE = 'Vehicle type is required';
export const PD_ID = 'Pdid is required';
export const ASSET_YEAR = 'Vehicle year is required';
export const REG_NO = 'Reg. No. is required';
export const FUEL_TYPE = 'Fuel type is required';
export const ASSET_MODEL = 'Vehicle model is required';
export const ASSET_MAKE = 'Make is required'
export const SIM_NO = 'SIM number should starts with 6789 (10)';
export const JIO_ID = 'Jio id is required';
export const ASSET_NAME_FORMAT_MSG = "Tag name only allow alphabets and numbers(50)";
export const REG_NO_FORMAT_MSG = "Reg. No. only allow alphabets and numbers(50)";
export const IMEI_FORMAT_MSG = "IMEI only allow numbers(15)";
export const IMSI_FORMAT_MSG = "IMSI only allow numbers(15)";
export const MODULE_TYPE = "Module type is required";


/*Login*/
export const TENENT_ID_MSG = "Tenant id is required"
export const SETEID_ID_MSG = "Site id is required"
export const CLIENT_NAME_MSG = "Client name is required"

//Service report
export const FROM_DATE_MSG = "From date is required";
export const TO_DATE_MSG = "To date is required";
export const PAGE_NO_MSG = "Page no is required";
export const PAGE_SIZE_MSG = "Page size is required";
export const REPORT_TYPE_MSG = "Report type is required";
export const REPORT_NAME_MSG = "Report name is required";
export const REPORT_STATUS_MSG = "Report status is required";
export const REPORT_DATETIME_MSG = "Report date time is required";
export const EXCEL_URL_MSG = "Excel url is required";
export const PDF_URL_MSG = "PDF url is required";
export const EXCEL_OR_PDF_URL_MSG = "PDF or excel url is required";
export const JIO_REPORT_ID_MSG = "JIO report id is required";

//Logs Messages
export const NOT_CREATED_MSG = "Not created";
export const CREATED_MSG = "Created";
export const NOT_DELETED_MSG = "Not deleted";
export const DELETED_MSG = "Deleted";
export const EXCEPTION_MSG = "Exception";


export const OBJECT_KEYS_MISMATCH_PDID = "Assign tracker object key pdid is mismatched";
export const OBJECT_KEYS_MISMATCH_NAME = "Assign tracker object key name is mismatched";
export const ASSIGN_TRACKER_MSG = "Assign tracker object is mismatched";
