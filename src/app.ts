import express from 'express'
import cors from 'cors';
import bodyParser from "body-parser";
const jwt = require("jsonwebtoken");
const mongoose=require("mongoose");

const app = express();
const PORT = 3002;

import employeesController from './controller/employeesController';
import problemsController from './controller/problemsController';






var dbPool: any;

app.use(bodyParser.json());
app.use(cors());
let logger: any;


mongoose.connect("mongodb+srv://nyc123:nyc1929@atlascluster.xupyk7f.mongodb.net/politicsMiniservices",{
    useNewUrlParser:true,
    useUnifiedTopology:true
},(err)=>{
    if(!err){
        console.log("connected to db")
    }else{
        console.log("error")
    }
});

var cron = require('node-cron');
cron.schedule('30 10 * * *', () =>{
    console.log("It's called cron job at 10:30")
})
// Schedule cron job to call the updateattendance API every day at 10:30 AM
    cron.schedule('30 10 * * *', async () => {
    try {
      let response = await fetch(' http://localhost:3000/attendence/updateattendance', { method: 'POST' });
      let data = await response.json();
      console.log(data.message);
    } catch (err) {
      console.error(err);
    }
  });
  
// cron.schedule('*/10 * * * * *', () => {
    //get moment current date and day
    // if day not equal to sunday or holiday or applied leaved (with approved)
    // get emplyees and check that employee in attendance collection with current date
    // if employee attendance no there , update checkin and and checkout as absent
    // if sunday/leave/holiday update related

//   console.log('running a task every minute');
// });

app.get('/testcss',function (req,res) {
    res.send("Api Called. It's working..")
})

var auth = function (req, res, next) {
    if (req.headers.userid != "" && req.headers.authorization){
    let jwtSecretKey = "nyc@12345";
  
  
    const verified = jwt.verify(req.headers.authorization, jwtSecretKey);
    if(verified){
      return next();
    }else{
        // Access Denied
        return res.status(401).send("token unautherized");
    }
  }else{
      return res.status(401).send("token unautherized..");
  }
  }


  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/employee',employeesController);
app.use('/pro',problemsController);


app.use('/Assets/', express.static('Assets'));










app.listen(PORT, () => {
    console.log("http://localhost:" + PORT)
})

export const dbPool1 = dbPool;
