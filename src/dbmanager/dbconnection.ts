import mongoose from 'mongoose';


var mongoDbConnection = async function (connectionString) {

    await mongoose.connect(connectionString,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex:true
        })
        .then(() => { console.log('Connected to database jiomotive'); })
        .catch(error => {
            console.error('Could not connect to database : ' + error)
            // setTimeout(mongoDbConnection, 5000);
        });
}

export async function mongoDBconnect(connectionString) {
    const mongoURI = connectionString + "?retryWrites=true&w=majority";
    mongoDbConnection(mongoURI);
}


