import mongoose from 'mongoose';

const employeessch = new mongoose.Schema({
    empid: String,
    firstname: String,
    surname: String,
    email: String,
    phone: String,
    gender: String,
    dob: String,
    password: String,
    ActualEmployeeType:String,
    role: {
        RoleName: String,
        RoleType: String,
        startdate: String,
        enddate: String,
    },
    SalaryDetails: {
        Salary: String,
        RoleType: String
    },
    FeeDetails: {
        NoofTerms: { type: Number },
        TotalFee: { type: Number },
        PaidFee: { type: Number },
        PendingFees: [
            {
                TermNo: { type: String },
                PendingFee: { type: Number },
            },
        ],
        TermwisePaidFees: [
            {
                TermNo: { type: String },
                PaidFee: { type: Number },
                PaidAmount: { type: Number },
                PaidDate: { type: Date },
                PaidStatus: String, 
            },
        ],
    },
        Technology: [
            {
                name: String,
                courses: String,
            }
        ],
  
    joiningdate: { type: Date },
    Groupid:[],
    CreatedBy: String,
    UpdatedBy: String,
    Address: {
        doorno: String,
        village: String,
        city: String,
        state: String,
        pincode: String
    },
    deletecomment: String,
    imgpath: String,
    imagename: String,
    AadharImagename: String,
    PanImagename: String,
    About:String,
    Status: String,
    CreateDate: { type: Date, default: Date.now },
    updateddate: { type: Date, default: Date.now }
}, {
    timestamps: true
});





// Create and export Device Alert model
export const employeesSchema = mongoose.model("employees", employeessch, "employees");

