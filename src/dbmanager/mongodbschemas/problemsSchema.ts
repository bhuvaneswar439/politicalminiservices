import mongoose from 'mongoose';

const problemSchema = new mongoose.Schema({
  name: String,
  mobileNumber: String,
  aadharNumber: String,
  file: String,
  comment: String
}, {
  timestamps: true
});

export const problemsSchema = mongoose.model("problems", problemSchema);
