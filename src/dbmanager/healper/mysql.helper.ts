import mysql from 'mysql';
class MySqlHelper {
    dbPool: any;
    isConRelease: boolean = true;
    establishMySqlConnection = async (mysqlOptions, logger?) => {
        this.dbPool = mysql.createPool(mysqlOptions);

        this.dbPool.on('acquire', function (connection) {
            // console.log('Connection %d acquired', connection.threadId);
        });

        this.dbPool.on('enqueue', function () {
            console.log('Waiting for available connection slot');
        });

        this.dbPool.on('release', function (connection) {
            // console.log('Connection %d released', connection.threadId);
        });

        this.dbPool.getConnection((err, connection) => {
            if (err) {

                if (connection) {
                    connection.release();
                }

                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.error('Database connection was closed.')
                }
                if (err.code === 'ER_CON_COUNT_ERROR') {
                    console.error('Database has too many connections.')

                }
                if (err.code === 'ECONNREFUSED') {
                    console.error('Database connection was refused.')

                }
                else {
                    console.error(err.message);
                    // setTimeout(function () {
                    //     dataBaseConnection();
                    // }, 5000)

                }
            }
            if (connection) {
                console.log("mysqldb connected")
                connection.release()
            }
            return
        })
    };



    public executeQueryWithCallBack = async (mySqlQuery, params, mySqlCallback): Promise<void> => {
        await this.dbPool.getConnection(async (err, connection) => {
            if (err) {
                if (connection) {
                    connection.release();
                }
                console.error(err.message);
            } else {
                await connection.query(mySqlQuery, params, (err, rows) => {
                    if (err) {
                        console.error(err.message);
                        mySqlCallback(err, rows);
                    } else {
                        // console.log(this.dbPool._freeConnections.indexOf(connection)); // -1
                        this.isConRelease ? connection.release() : connection.destroy();
                        // console.log(this.dbPool._freeConnections.indexOf(connection)); // 0
                        // reslove(rows);
                        mySqlCallback(null, rows);
                    }
                });
            }

        });
    }

    public executeQueryWithOutParams = async (mySqlQuery, mySqlCallback): Promise<void> => {
        await this.dbPool.getConnection(async (err, connection) => {
            if (err) {
                if (connection) {
                    connection.release();
                }
                console.error(err.message);
            } else {
                await connection.query(mySqlQuery, (err, rows) => {
                    if (err) {
                        console.error(err.message);
                        mySqlCallback(err, rows);
                    } else {
                        // console.log(this.dbPool._freeConnections.indexOf(connection)); // -1
                        this.isConRelease ? connection.release() : connection.destroy();
                        // console.log(this.dbPool._freeConnections.indexOf(connection)); // 0
                        // reslove(rows);
                        mySqlCallback(null, rows);
                    }
                });
            }

        });
    }
};
const mySqlHelper = new MySqlHelper();
export default mySqlHelper;